---
title: "Aula 07"
author: "Saulo"
date: "13 de outubro de 2017"
output: word_document
---

```{r}
install.packages("stringr")
library(stringr)
nome <- c("Saulo")
str_length(nome)

nome_completo <- c("Saulo Quadros Santiago")
str_length(nome_completo)


```

```{r}
classico <- read.table("C:/Users/treinamento/Desktop/cursoR_itermed_Ipea/bancos/Aula 4/cancao_do_exilio.txt")
```

Alternativa:

```{r}
classico <- read.table("./bancos/Aula 4/cancao_do_exilio.txt")
```

```{r}
typeof(classico)
```
```{r}
classico$caracters <- str_length(classico$verso)
classico <- classico %>% 
  dplyr::mutate(caracters2 = str_length(verso))
classico$caracter3 <- apply(as.data.frame(as.character(classico$verso)), 1, str_length)
```

Trabalhando com a funÃ§Ã£o *apply*

```{r}
dados <- as.data.frame(matrix(rnorm(5000,10,5), nrow=1000,ncol=5))

somaLinhas <- apply(dados,1,sum)
mediaColunas <- apply(dados,2,mean)
knitr::kable(mediaColunas)
normalizaColunas <- apply(dados,2,function(x) (x-min(x))/(max(x)-min(x)))

```

```{r}
str_c("ABC", "abc", "123")
#A separatriz entre os elementos pode ser definida livremente
str_c("ABC", "abc", "123", sep = " ")
str_c("ABC", "abc", "123", sep = "[qualquer coisa]")
```

Vetores de strings tambÃ©m podem ser alimentados na funÃ§Ã£o str_c()

```{r}
lista_palavras <- c("informa","atualiza","organiza","ocupa") 
str_c("Des", lista_palavras, "do")

```
Um vetor de strings pode ser convertido numa Ãºnica string com o argumento collapse:

```{r}
str_c(str_c("Des", lista_palavras, "do"), collapse = '. ')

```












