---
title: "Aula 6"
author: "Saulo"
date: "10 de outubro de 2017"
output:
  word_document: default
  pdf_document: default
  html_document: default
---



```{r}
library(nycflights13)
library(dplyr)
library(knitr)
data("flights")

flights %>% 
  select(tailnum, dest) %>% 
  group_by(tailnum) %>% 
  summarise(ndest = n_distinct(dest)) %>% 
  filter(ndest == 1) %>% 
  kable()

  
```

# Qual o destino mais visitado por companhia aérea (carrier)?

```{r}
flights %>% 
  group_by(carrier, dest) %>% 
  summarise(n = n()) %>% 
  mutate (rank = rank(desc(n))) %>% 
  filter (rank == 1) %>% 
  head (n = 3) %>% 
  kable()
```

```{r, warning = F, message = F, echo = F, eval = T}
flights %>% 
  filter (!is.na(dep_time) & dep_time >arr_time) %>% 
  summarise(n_flights = n()) %>% 
  kable()
```

## Começando a trabalhar com *gather*.

```{r, warning = F, message = F, echo = F, eval = T}
head(mtcars, 5) %>% 
  kable(caption = "Tabela mtcars")

```


Estudando a função **gather**

```{r}
mtcars %>% 
  tidyr::gather("peca", "n", cyl, gear, carb) %>% 
  dplyr::group_by(peca) %>% 
  dplyr::summarise(n_distintos = n_distinct(n)) %>% 
  kable ()
```

Função *spread*, inversa da *gather*.

```{r}
mtcars %>% 
  tidyr::gather("peca", "n", cyl, gear, carb) %>% 
  dplyr::group_by(peca) %>% 
  dplyr::summarise(n_distintos = n_distinct(n)) %>% 
  tidyr::spread(peca, n_distintos) %>% 
  kable ()
```

#Trabalhando com **starwars**.

Mais um exemplo de como criar paineis.

```{r}
data("starwars")
starwars %>% 
  tidyr::gather("tipo", "cor",
                dplyr::ends_with("color")) %>%
  dplyr::group_by(tipo) %>% 
  summarise(cores_distintas = n_distinct(cor)) %>% 
  kable()
  
```

Criando mais um código com **starwars**

```{r}
dplyr::starwars %>% 
  dplyr::filter(gender %in% c("male", "female")) %>% 
  tidyr::spread(gender, height) %>% 
  select(male, female) %>% 
  head() %>% 
  kable()
```

#Exemplo de base usando nycflights

```{r, warning = F, message = F, echo = F, eval = T}
flights %>% 
  tidyr::unite("trajetos", origin, dest, sep = " - ") %>% 
  dplyr::group_by(carrier) %>% 
  dplyr::summarise(trajetos_distintos = 
                     n_distinct(trajetos)) %>% 
  head() %>% 
  kable()

```

Instalar pacote *stringr*

```{r}
install.packages("stringr")
library(stringr)
```







