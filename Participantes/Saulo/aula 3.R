#Exemplo Histograma
dados <- rnorm(1000,10,3)
hist(dados)

#Teste Saulo
numero <- rnorm(1000)
base.df <- read.table(file.choose(), sep="&", header = TRUE)
View(base.df)

#Comandos para cria??o de data frame no R
#read.table () ? um leitor mais geral, separado por qualquer tipo de separador
?read.table
#Primeira maneira
base.df<-read.table("C:/Users/treinamento/Desktop/cursoR_itermed_Ipea/Participantes/Roberta da Silva Vieira/Base.txt", sep="&", header=TRUE)
#Segunda maneira
base.df<-read.table("Participantes/Roberta da Silva Vieira/Base.txt", sep="&", header=TRUE)
#Terceira maneira
base.df<-read.table(file.choose(), sep="&", header=TRUE)

#Se o arquivo ? separado por ";", usar a separa??o read.csv(), se o arquivo ? separado por ",", usar o read.csv2()

#As bases do IBGE s?o lidas com read_fwf para dizer a posi??o

#instalar o pacote devtools 
install.packages("devtools")
#pacote microdadosBrasil ? um pacote para ler as bases da PNAD, POF, Censo desenvolvido pelo Lucas e Daniel.
devtools::install_github("lucasmation/microdadosBrasil")

#ou 
library(devtools)
help("install_github")
install_github("lucasmation/microdadosBrasil")

#Intalar pacote readr
install.packages("readr")
library(readr)

#Ler PNAD
dic <- readr::read_csv("bancos/dicionario_pessoas.csv")
dic
str(dic)

###Questão 2
help("read_fwf")
help("fwf_widths")
widths <- readr::fwf_widths(widths = dic$tamanho2,
                            col_names=dic$cod2)
str("widths")

pnad09.df <- readr::read_fwf(file = "bancos/AMOSTRA_DF_PNAD2009P.txt",
                             col_positions = widths)

###Questão 3
pnad_microBr <- microdadosBrasil::read_PNAD(ft = "pessoas",
                                            i = 2009,
                                            file = "bancos/AMOSTRA_DF_PNAD2009p.txt")


#Leitura de arquivos Excel
install.packages("readxl")
library(readxl)
excel_sheets("bancos/datasets.xls")

##Questão 2
chickwts.df <- read_excel(path = "bancos/datasets.xls",
                          sheet = 3)

###Questão 3
files <- "bancos/datasets.xls"
planilhas <- lapply(excel_sheets(files),
                    function(x) read_excel(files, sheet = x))
str(planilhas)
iris <- planilhas[[1]]
mtcars<- planilhas[[2]]
chickwts<- planilhas[[3]]
quakes<- planilhas[[4]]
